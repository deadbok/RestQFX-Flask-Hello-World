# Hello World Flask application

This is an Hello World app using Flask and Python, used to test the Docker
container for the RestQFX project.

## Building as package

setup.py contains the metadata for building the package.

    ./build.sh

## Running

After installing the package run `hello` script run the application using the
Flask built-in server.
