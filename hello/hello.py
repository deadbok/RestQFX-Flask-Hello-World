from hello import app

# This is read by setup.py

_version_ = '1.0.0'

@app.route('/')
def very_nice_function_name():
    return 'Hello World!'
