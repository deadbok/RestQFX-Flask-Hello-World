import os
import re
import sys
import codecs
from os import path
from distutils.sysconfig import get_python_lib
from setuptools import find_packages, setup

# Curren absolute path, used later on to not get lost.
here = path.abspath(path.dirname(__file__))

# Files and directories to exclude from the package.
EXCLUDE_FROM_PACKAGES = []

# Read the version number from a source file.
# Why read it, and not import?
# see https://groups.google.com/d/topic/pypa-dev/0PkjVpcxTzQ/discussion
def find_version(*file_paths):
    '''
    Use a regular expression to look for a variable named `_version_` in a given
    file.
    '''
    # Open in Latin-1 so that we avoid encoding errors.
    # Use codecs.open for Python 2 compatibility
    with codecs.open(os.path.join(here, *file_paths), 'r', 'latin1') as f:
        version_file = f.read()

    # The version line must have the form __version__ = 'ver'
    version_match = re.search(r"^_version_ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)

    # Complain if no version was found.
    if version_match:
        return version_match.group(1)
    raise RuntimeError("Unable to find version string.")

# Get the long description from the relevant file
with codecs.open('README.rst', encoding='utf-8') as desc_file:
    long_description = desc_file.read()

# Setup function used to define the package meta data
setup(
    # Package name.
    name='flask-hello-world',

    # Package version. either a string like `'1.0.1'` or like this, using the
    # find_version function to lookup the `_version_` variable in
    # `hello/hello.py`
    version=find_version('hello/hello.py'),

    # Source URL of the package.
    url='https://gitlab.com/ITT-16A/RestQFX',

    # Author data.
    author='ITT at EAL Odense 2017',
    author_email='somebody@eal.dk',

    # Description is either a string or, like here, read from the `README.rst`
    # file.
    description=long_description,

    # License
    license='Beerware',

    # Automatically find the packages to include.
    packages=['hello'],

    # Script installed in a standard location to run the application (not needed
    # for libraries)
    scripts=['bin/hello'],

    # The package requires Flask.
    install_requires=['Flask'],

    # We want a version equal to or newer that Python 3.
    python_requires='>=3',

    # Clasifiers for PyPi (not used when installing), look them up at:
    #  https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
    # How mature is this project? Common values are
    #   3 - Alpha
    #   4 - Beta
    #   5 - Production/Stable
    'Development Status :: 3 - Alpha',

    'Framework :: Flask',
    'Topic :: Automation :: Build Tools',
    'Topic :: System :: Networking',

    # Pick your license as you wish (should match "license" above)
    'License :: Beerware',

    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.6'
],
)
